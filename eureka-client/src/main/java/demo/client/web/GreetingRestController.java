package demo.client.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@RestController
public class GreetingRestController {
    private final AtomicInteger counter = new AtomicInteger(0);

    @GetMapping("/hi/{name}")
    public Map<String, String> hi(@PathVariable String name,
                                  @RequestHeader(value = "X-CNJ-Name", required = false)
                                          Optional<String> cn) {
        String resolvedName = cn.orElse(name);
        log.info("request #{}", counter.incrementAndGet());
        return Collections.singletonMap("greeting", "Hello, " + resolvedName + "!");
    }
}
