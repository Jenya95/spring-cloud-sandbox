package demo.edge.clr;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Map;

@Slf4j
@Component
public class LoadBalancedRestTemplateCLR implements CommandLineRunner {
    private final RestTemplate restTemplate;

    @Autowired
    public LoadBalancedRestTemplateCLR(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public void run(String... args) throws Exception {
        Map<String, String> variables = Collections.singletonMap("name", "Cloud Natives");
        for (int i = 0; i < 5000; i++) {
            ResponseEntity<JsonNode> response =
                    restTemplate.getForEntity("//greeting-service/hi/{name}", JsonNode.class, variables);
            JsonNode body = response.getBody();
            String greeting = body.get("greeting").asText();
            log.info("greeting : {}", greeting);
        }
    }
}
