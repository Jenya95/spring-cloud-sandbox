package demo.edge.clr;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DiscoveryClientCLR implements CommandLineRunner {
    private final DiscoveryClient discoveryClient;

    @Autowired
    public DiscoveryClientCLR(DiscoveryClient discoveryClient) {
        this.discoveryClient = discoveryClient;
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("localServiceInstance");
        log.info("{}", discoveryClient.getServices());
        String serviceId = "greeting-service";
        log.info("Registered instances of {}");
        discoveryClient.getInstances(serviceId).forEach(this::logServiceInstance);
    }

    private void logServiceInstance(ServiceInstance si) {
        log.info("hast = {}, post = {}, service id = {}",
                si.getHost(),
                si.getPort(),
                si.getServiceId());
    }

}
